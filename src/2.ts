const redLimit = 12;
const greenLimit = 13;
const blueLimit = 14;

export const sumOfPossibleGames = (games: string) => {
  const possibleGames: number[] = [];
  const gamesArray = games.split("\n");

  for (const game of gamesArray) {
    const [gameInfos, gameResult] = game.split(":");
    const gameId = Number(gameInfos.split(" ").at(1));
    const maxRed = findColorMaxValue(" red", gameResult, 0)[1];
    if (maxRed > redLimit) {
      continue;
    }
    const maxGreen = findColorMaxValue(" green", gameResult, 0)[1];
    if (maxGreen > greenLimit) {
      continue;
    }
    const maxBlue = findColorMaxValue(" blue", gameResult, 0)[1];
    if (maxBlue > blueLimit) {
      continue;
    }
    possibleGames.push(gameId);
  }
  console.log(possibleGames);
  return possibleGames.reduce((acc, curr) => acc + curr, 0);
};

export const sumOfPowerOfAllSets = (games: string) => {
  const possibleGames: number[] = [];
  const gamesArray = games.split("\n");

  for (const game of gamesArray) {
    const maxRed = findColorMaxValue(" red", game, 0)[1];
    const maxGreen = findColorMaxValue(" green", game, 0)[1];
    const maxBlue = findColorMaxValue(" blue", game, 0)[1];
    possibleGames.push(maxRed * maxBlue * maxGreen);
  }
  return possibleGames.reduce((acc, curr) => acc + curr, 0);
};

const findColorMaxValue = (
  match: string,
  input: string,
  maxValue: number
): [string, number] => {
  const firstRedIndex = input.indexOf(match);

  if (firstRedIndex === -1) {
    return [input, maxValue];
  }
  const substr = input.substring(0, firstRedIndex);
  const lastSpaceIndex = substr.lastIndexOf(" ");
  const gameValue = Number(substr.substring(lastSpaceIndex + 1));
  if (gameValue > maxValue) {
    maxValue = gameValue;
  }
  const inputString = input.replace(match, "");

  return findColorMaxValue(match, inputString, maxValue);
};
