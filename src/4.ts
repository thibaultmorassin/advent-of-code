export const computeWinningPoints = (games: string) => {
  const gamesArray = games.split("\n");

  let gamePoints = 0;
  // let multiplier: { [value: string]: number } = {};
  let multiplier: { [value: string]: number } = {};

  for (const game of gamesArray) {
    let currentGamePoints = 0;
    const [gameInfos, gameCard] = game.split(":");
    const [winningNumbers, gameResult] = gameCard.split("|");

    const gameIndex = Number(gameInfos.split(" ").at(-1));

    console.log(`GAME ${gameIndex}`);
    for (const num of gameResult.split(" ")) {
      if (!num) {
        continue;
      }
      if (winningNumbers.includes(` ${num} `)) {
        currentGamePoints += 1;
      }
    }
    console.log(`+ ${currentGamePoints} PTS`);

    if (!multiplier[gameIndex]) {
      multiplier[gameIndex] = 1;
    }
    const currentGameMultiplier = multiplier[gameIndex];
    for (let i = 1; i <= currentGamePoints; i++) {
      const nextGameMultiplier = multiplier[gameIndex + i] ?? 1;

      multiplier = {
        ...multiplier,
        [gameIndex + i]: nextGameMultiplier + currentGameMultiplier,
      };
    }
    // console.log(multiplier);
    console.log(" ");
  }

  return Object.values(multiplier).reduce((acc, curr) => acc + curr, 0);
};
