const sumOfAllCalibrationValue = (text: string): number => {
  let res = 0;

  const wordToDigitMap: { [key: string]: string } = {
    one: "1",
    two: "2",
    three: "3",
    four: "4",
    five: "5",
    six: "6",
    seven: "7",
    eight: "8",
    nine: "9",
  };

  const replacedStr = text.replace(
    /one|two|three|four|five|six|seven|eight|nine/g,
    (match: string) => {
      return wordToDigitMap[match.toLowerCase()];
    }
  );

  replacedStr.split("\n").forEach((line) => {
    const filteredLine = line.replace(/[^1-9]/g, "");

    const lineValue = Number(
      `${filteredLine.split("").at(0)}${filteredLine.split("").at(-1)}`
    );

    res += lineValue;
  });
  return res;
};

export default sumOfAllCalibrationValue;
